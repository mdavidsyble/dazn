package com.assessment.dazn.ui.home

import com.assessment.dazn.utils.Resource
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.assessment.dazn.data.api.match.MatchesApiHelper
import com.assessment.dazn.data.model.match.MatchModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private var apiService: MatchesApiHelper
) : ViewModel() {

     val matches = MutableLiveData<Resource<List<MatchModel>>>()
    private var page = 1
    private var limit = 20
    var isRefresh: Boolean = false
    init {
        fetchMatches()
    }
      private fun fetchMatches() {
        viewModelScope.launch {
            matches.postValue(Resource.loading(null))
            try {
                val options = mapOf("page" to page.toString(), "limit" to limit.toString())
                val result = apiService.getMatches( options)
                matches.postValue(Resource.success(result.body()))
            } catch (e: Exception) {
                matches.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    fun fetchMatchesRefresh() {
        viewModelScope.launch {
            isRefresh = true
            page = 1
            try {
                val options = mapOf("page" to page.toString(), "limit" to limit.toString())
                val result = apiService.getMatches( options)
                matches.postValue(Resource.success(result.body()))
            } catch (e: Exception) {
                matches.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    fun fetchMoreMatches() {
        viewModelScope.launch {

            try {
                val options = mapOf("page" to (page + 1).toString(), "limit" to limit.toString())
                val result = apiService.getMatches(options)
                page++
                matches.postValue(Resource.success(result.body()))
            } catch (e: Exception) {
                matches.postValue(Resource.error(e.toString(), null))
            }
        }
    }

}