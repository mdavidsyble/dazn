package com.assessment.dazn.ui.schedule

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.assessment.dazn.data.model.schedule.ScheduleModel
import com.assessment.dazn.databinding.FragmentScheduleBinding
import com.assessment.dazn.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ScheduleFragment: Fragment() {

    private var _binding: FragmentScheduleBinding? = null

    private val binding get() = _binding!!
    private val viewModel: ScheduleViewModel by viewModels()
    @Inject
    lateinit var adapter: ScheduleAdapter

    private lateinit var recyclerView: RecyclerView
    private lateinit var nestedSV: NestedScrollView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentScheduleBinding.inflate(inflater, container, false)
        val root: View = binding.root

        recyclerView = binding.recyclerView
        progressBar = binding.progressBar
        swipeRefreshLayout = binding.swipeRefresh
        nestedSV = binding.idNestedSV

        setupUI()
        setupObserver()
        setupRefresh()
        setupPagination()
        return root
    }

    private fun setupUI() {
        recyclerView.layoutManager = LinearLayoutManager(this.context)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapter
        adapter.setOnItemClickListener(object : ScheduleAdapter.OnItemClickListener{
            override fun onItemClick(item: ScheduleModel, position: Int) {
                Toast.makeText(context, "Schedule selected: ${item.title}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setupRefresh(){
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.fetchScheduleRefresh()
        }
    }

    private fun setupPagination(){
        nestedSV.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                progressBar.visibility = View.VISIBLE
                viewModel.fetchMoreSchedule()
            }
        })
    }


    private fun setupObserver() {
        viewModel.schedules.observe(viewLifecycleOwner) {
            when (it.status) {

                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    swipeRefreshLayout.isRefreshing = false

                    it.data?.let { schedules -> renderList(schedules) }
                    recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    swipeRefreshLayout.isRefreshing = false
                    recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressBar.visibility = View.GONE
                    swipeRefreshLayout.isRefreshing = false
                    Toast.makeText(this.context, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun renderList(users: List<ScheduleModel>) {
        if (viewModel.isRefresh){
            adapter.addData(users, true)
            viewModel.isRefresh = false
        }else{
            adapter.addData(users, false)
        }
        adapter.notifyDataSetChanged()
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}