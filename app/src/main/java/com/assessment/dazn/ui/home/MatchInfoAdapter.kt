package com.assessment.dazn.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.assessment.dazn.R
import com.assessment.dazn.data.model.match.MatchModel
import com.assessment.dazn.databinding.ItemLayoutBinding
import com.assessment.dazn.utils.AppCommons
import com.bumptech.glide.Glide

class MatchInfoAdapter(
    private val users: ArrayList<MatchModel>,
) : RecyclerView.Adapter<MatchInfoAdapter.DataViewHolder>() {

    private lateinit var mListener: OnItemClickListener

    interface OnItemClickListener{
        fun onItemClick(item: MatchModel, position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener){
        mListener = listener
    }
    class DataViewHolder(
        val binding: ItemLayoutBinding,
        listener: OnItemClickListener,
        val users: ArrayList<MatchModel>,
    ) : RecyclerView.ViewHolder(binding.root){
        init {

            binding.root.setOnClickListener{
                listener.onItemClick(users[bindingAdapterPosition], bindingAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val binding = ItemLayoutBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return DataViewHolder(binding, mListener, users)
    }


    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        with(holder){
            with(users[position]){
                binding.textViewMatchTitle.text = this.title
                binding.textViewMatchSubTitle.text = this.subtitle
                binding.textViewDateInfo.text = AppCommons().formatDate(this.date)

                Glide.with(binding.matchImage.context)
                    .load(this.imageUrl)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.matchImage)
            }
        }
    }

    override fun getItemCount(): Int = users.size

    fun addData(list: List<MatchModel>, isRefresh: Boolean = false) {
        if(isRefresh)
           users.clear()
        users.addAll(list)
       // users.sortBy {  }
        users.sortWith { o1, o2 ->
            o2.date.compareTo(
                o1.date
            )
        }
    }

}