package com.assessment.dazn.ui.player

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.assessment.dazn.data.model.match.MatchModel
import com.assessment.dazn.databinding.MainVideoPlayerBinding
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.StyledPlayerView
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainVideoPlayerPage : AppCompatActivity() {

    private lateinit var binding: MainVideoPlayerBinding

    private lateinit var exoPlayerView: StyledPlayerView
    private lateinit var exoPlayer: ExoPlayer


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainVideoPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val title: String? = intent.getStringExtra("title")
        val videoUrl: String? = intent.getStringExtra("videoUrl")
        val subtitle: String? = intent.getStringExtra("subtitle")

        this.title = title;
        binding.idTVHeading.text = subtitle

        exoPlayerView = binding.idExoPlayerVIew
        try {
            val mediaItem: MediaItem = MediaItem.fromUri(videoUrl!!)
            exoPlayer = ExoPlayer.Builder(this).build()

            exoPlayerView.player = exoPlayer
            exoPlayer.repeatMode = Player.REPEAT_MODE_ALL
            exoPlayer.setMediaItem(mediaItem)
            exoPlayer.prepare()
            exoPlayer.playWhenReady = true

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        exoPlayer.stop()
    }
}
