package com.assessment.dazn.ui.schedule

import android.os.Handler
import android.os.Looper
import com.assessment.dazn.utils.Resource
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.assessment.dazn.data.api.match.MatchesApiHelper
import com.assessment.dazn.data.api.match.RetrofitBuilder
import com.assessment.dazn.data.model.schedule.ScheduleModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ScheduleViewModel @Inject constructor(
    private var apiService: MatchesApiHelper
) : ViewModel() {
    val schedules = MutableLiveData<Resource<List<ScheduleModel>>>()
    private var page = 1
    private var limit = 20
    var isRefresh: Boolean = false
    val mainHandler = Handler(Looper.getMainLooper())
    init {
        fetchSchedules()
        mainHandler.post(object : Runnable {
            override fun run() {
                fetchScheduleRefresh()
                mainHandler.postDelayed(this, 30000)
            }
        })
    }

    private fun fetchSchedules() {

        viewModelScope.launch {
            schedules.postValue(Resource.loading(null))
            try {
                val options = mapOf("page" to page.toString(), "limit" to limit.toString())
                val result = apiService.getSchedules( options)
                schedules.postValue(Resource.success(result.body()))
            } catch (e: Exception) {
                schedules.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    fun fetchScheduleRefresh() {
        viewModelScope.launch {
            // users.postValue(Resource.loading(null))
            isRefresh = true
            page = 1
            try {
                val options = mapOf("page" to page.toString(), "limit" to limit.toString())
                val result = apiService.getSchedules( options)
                schedules.postValue(Resource.success(result.body()))
            } catch (e: Exception) {
                schedules.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    fun fetchMoreSchedule() {
        viewModelScope.launch {

            try {
                val options = mapOf("page" to (page + 1).toString(), "limit" to limit.toString())
                val result = apiService.getSchedules(options)
                page++
                schedules.postValue(Resource.success(result.body()))
            } catch (e: Exception) {
                schedules.postValue(Resource.error(e.toString(), null))
            }
        }
    }
}