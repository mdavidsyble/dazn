package com.assessment.dazn.ui.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.assessment.dazn.data.api.match.MatchesApiHelper
import com.assessment.dazn.data.model.match.MatchModel
import com.assessment.dazn.databinding.FragmentHomeBinding
import com.assessment.dazn.ui.player.MainVideoPlayerPage
import com.assessment.dazn.utils.Status
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment: Fragment()  {

    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding!!
    private val viewModel: HomeViewModel  by viewModels()
    @Inject
    lateinit var adapter: MatchInfoAdapter

    private lateinit var recyclerView: RecyclerView
    private lateinit var nestedSV: NestedScrollView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

      recyclerView = binding.recyclerView
       progressBar = binding.progressBar
        swipeRefreshLayout = binding.swipeRefresh
        nestedSV = binding.idNestedSV

        setupUI()
        setupObserver()
        setupRefresh()
        setupPagination()
        return root
    }

    private fun setupUI() {
        recyclerView.layoutManager = LinearLayoutManager(this.context)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapter
        adapter.setOnItemClickListener(object : MatchInfoAdapter.OnItemClickListener{
            override fun onItemClick(item: MatchModel, position: Int) {
                goToVideoPlayer(item)
            }

        })
    }

    fun goToVideoPlayer(item: MatchModel) {
        val intent = Intent(this.context, MainVideoPlayerPage::class.java)
        intent.putExtra("title", item.title)
        intent.putExtra("videoUrl", item.videoUrl)
        intent.putExtra("subtitle", item.subtitle)
        startActivity(intent)
    }

    private fun setupRefresh(){
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.fetchMatchesRefresh()
        }
    }

    private fun setupPagination(){
        nestedSV.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                progressBar.visibility = View.VISIBLE
                viewModel.fetchMoreMatches()
            }
        })
    }


    private fun setupObserver() {
        viewModel.matches.observe(viewLifecycleOwner) {
            when (it.status) {

                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    swipeRefreshLayout.isRefreshing = false

                    it.data?.let { matches -> renderList(matches) }
                    recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    swipeRefreshLayout.isRefreshing = false
                    recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressBar.visibility = View.GONE
                    swipeRefreshLayout.isRefreshing = false
                    Toast.makeText(this.context, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun renderList(users: List<MatchModel>) {
        if (viewModel.isRefresh){
           adapter.addData(users, true)
            viewModel.isRefresh = false
        }else{
            adapter.addData(users, false)
        }
        adapter.notifyDataSetChanged()
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}