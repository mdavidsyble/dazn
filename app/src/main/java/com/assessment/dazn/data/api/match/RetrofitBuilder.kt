package com.assessment.dazn.data.api.match

import com.assessment.dazn.utils.AppConstants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitBuilder {
    private fun getInstance(): Retrofit {
        return Retrofit.Builder().baseUrl(AppConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideApiService(): MatchesApiHelper = getInstance().create(MatchesApiHelper::class.java)


    val apiService: MatchesApiHelper = getInstance().create(MatchesApiHelper::class.java)

}