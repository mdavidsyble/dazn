package com.assessment.dazn.data.model.schedule

data class ScheduleModel(
    val id: Int = 0,
    val title: String = "",
    val subtitle: String = "",
    val date: String = "",
    val imageUrl: String = "",
)
