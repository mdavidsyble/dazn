package com.assessment.dazn.data.model.match

data class MatchResponse(
    val count: Int,
    val lastItemIndex: Int,
    val page: Int,
    val results: List<MatchModel>,
    val totalCount: Int,
    val totalPages: Int
)