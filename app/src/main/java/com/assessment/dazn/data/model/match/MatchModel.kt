package com.assessment.dazn.data.model.match


data class MatchModel(
    val id: Int = 0,
    val title: String = "",
    val subtitle: String = "",
    val date: String = "",
    val imageUrl: String = "",
    val videoUrl: String = "",
)
