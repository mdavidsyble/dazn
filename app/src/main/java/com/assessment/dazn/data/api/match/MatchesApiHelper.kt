package com.assessment.dazn.data.api.match

import com.assessment.dazn.data.model.match.MatchModel
import com.assessment.dazn.data.model.schedule.ScheduleModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface MatchesApiHelper {
    @GET("/getEvents")
    suspend fun getMatches(@QueryMap options: Map<String, String>) : Response<List<MatchModel>>

    @GET("/getSchedule")
    suspend fun getSchedules(@QueryMap options: Map<String, String>) : Response<List<ScheduleModel>>
}