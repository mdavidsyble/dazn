package com.assessment.dazn.data.model.schedule

data class ScheduleResponse(
    val count: Int,
    val lastItemIndex: Int,
    val page: Int,
    val results: List<ScheduleModel>,
    val totalCount: Int,
    val totalPages: Int
)