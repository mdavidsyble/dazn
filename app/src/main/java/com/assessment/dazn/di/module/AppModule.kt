package com.assessment.dazn.di.module

import android.content.Context
import com.assessment.dazn.BaseApplication
import com.assessment.dazn.data.model.match.MatchModel
import com.assessment.dazn.data.model.schedule.ScheduleModel
import com.assessment.dazn.ui.home.MatchInfoAdapter
import com.assessment.dazn.ui.schedule.ScheduleAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext app: Context): BaseApplication{
        return app as BaseApplication
    }

    @Singleton
    @Provides
    fun provideInitialMatchesList(): ArrayList<MatchModel>{
    return arrayListOf()
    }

    @Singleton
    @Provides
    fun provideInitialScheduleList(): ArrayList<ScheduleModel>{
        return arrayListOf()
    }

    @Singleton
    @Provides
    fun provideMatchesAdapter(matches: ArrayList<MatchModel>): MatchInfoAdapter{
        return MatchInfoAdapter(matches)
    }

    @Singleton
    @Provides
    fun provideScheduleAdapter(schedules: ArrayList<ScheduleModel>): ScheduleAdapter{
        return ScheduleAdapter(schedules)
    }
}
