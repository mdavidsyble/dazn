package com.assessment.dazn.utils
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}