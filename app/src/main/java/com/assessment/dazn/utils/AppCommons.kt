package com.assessment.dazn.utils

import android.text.format.DateUtils
import org.ocpsoft.prettytime.PrettyTime
import java.text.SimpleDateFormat
import java.util.*


class AppCommons {
    fun formatDate (
        date: String): String{

        val finalDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
            .parse(date)
        val simpleDate = SimpleDateFormat("dd.MM.yyyy HH:mm").format(finalDate!!)

         val isToday = DateUtils.isToday(finalDate.time)
        if (isToday){
            val timePast = hoursPast(finalDate)
            return if(timePast in 0..1){
                val prettyTime = PrettyTime()
               prettyTime.format(finalDate)
            }else{
                "Today, ${SimpleDateFormat("HH:mm").format(finalDate)}"
            }
        }

        return simpleDate
    }

    private fun hoursPast (date: Date): Int {
        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val timePast = Date().time - date.time

        val elapsedHours: Long = timePast / hoursInMilli
        return elapsedHours.toInt()
    }
}