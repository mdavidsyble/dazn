package com.assessment.dazn

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ActivityScenario.launch
import com.assessment.dazn.data.api.match.RetrofitBuilder
import com.assessment.dazn.data.model.match.MatchModel
import com.assessment.dazn.data.model.schedule.ScheduleModel
import com.assessment.dazn.di.module.AppModule
import com.assessment.dazn.ui.home.HomeFragment
import com.assessment.dazn.ui.home.HomeViewModel
import com.assessment.dazn.ui.schedule.ScheduleViewModel
import com.assessment.dazn.utils.Resource
import com.assessment.dazn.utils.Status
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@UninstallModules(AppModule::class)
@HiltAndroidTest
class MainTest {
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var initialList:  ArrayList<MatchModel>

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var matchResponse: Resource<List<MatchModel>>
    private var matchesList = mutableListOf<MatchModel>()

    private val retroBuilder = mockk<RetrofitBuilder>(relaxed = true)

    private lateinit var scheduleViewModel: ScheduleViewModel
    private var scheduleList = mutableListOf<ScheduleModel>()
    private lateinit var scheduleResponse: Resource<List<ScheduleModel>>

    @Before
    fun init(){
        hiltRule.inject()
        //home view model
        homeViewModel = HomeViewModel(retroBuilder.apiService)
        matchesList.add(MatchModel(1,"Soccer 1","","","",""))
        matchesList.add(MatchModel(2,"Soccer 2","","","",""))
        matchesList.add(MatchModel(3,"Soccer 3","","","",""))
        matchesList.add(MatchModel(4,"Soccer 4","","","",""))
        matchesList.add(MatchModel(5,"Soccer 4","","","",""))
        matchResponse = Resource.success(matchesList)
        //schedule view model
        scheduleViewModel = ScheduleViewModel(retroBuilder.apiService)
        scheduleList.add(ScheduleModel(1,"Soccer 1","","",""))
        scheduleList.add(ScheduleModel(2,"Soccer 2","","",""))
        scheduleList.add(ScheduleModel(3,"Soccer 3","","",""))
        scheduleList.add(ScheduleModel(4,"Soccer 4","","",""))
        scheduleList.add(ScheduleModel(5,"Soccer 4","","",""))
        scheduleResponse = Resource.success(scheduleList)
    }

    @Test
    fun testInitialAppState(){
       assert(initialList.isEmpty())
    }

    @Test
    fun testHomeViewModel(){
        homeViewModel.matches.postValue(matchResponse)
        val results = homeViewModel.matches.getOrAwaitValue()
        assert(!homeViewModel.isRefresh)
        assertEquals(results.data!!.size, 5)
        assertEquals(results.status, Status.SUCCESS)
        assertEquals(results.data!!.first().id, 1)
    }

    @Test
    fun testScheduleViewModel(){
        scheduleViewModel.schedules.postValue(scheduleResponse)
        val results = scheduleViewModel.schedules.getOrAwaitValue()
        assert(!scheduleViewModel.isRefresh)
        assertEquals(results.data!!.size, 5)
        assertEquals(results.status, Status.SUCCESS)
        assertEquals(results.data!!.first().id, 1)
    }

    @Test
    fun mainActivityTest(){
       val scenario = launch(MainActivity::class.java)


    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun homeFragmentTest(){
        val scenario = launchFragmentInHiltContainer<HomeFragment>{}

    }

}


