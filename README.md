# Simple Android Project for DAZN Assessment

This repository contains a simple implementation of the MVVM architecture in Kotlin

# Project Structure

#### The app has following packages:
1. **data**: It contains all the data accessing and manipulating components.
2. **di**: Dependency providing classes using Hilt. Note that some dependency providing classes may be in other project directory.
3. **ui**: View classes along with their corresponding Presenters.
4. **utils**: Utility classes.

The project users
1. Hilt
2. Coroutines
3. Retrofit

#### Classes have been designed in such a way that it could be inherited and maximize the code reuse.


